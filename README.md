# Arduino Communication Protocol Project

### Basic Project Description:
* Developing communication between the Arduino Board and the different Sensor Units.
* Developing a communication protocol between the Arduino Board and the Main Control Panel using RS485 physical protocol, and ModBus message communication protocol.


## Full Project Description.

### Sensor Units:
* Made out of a few different sensors:
    * 3 Temperature sensors
    * 2 Ground Moisture Sensors.
    * 1 Light level sensor.
* 4 Sensor Units are connected to each Arduino Board.
* The sensor values are read every time the Main Control Panel communicates with the Arduino Board.
* The Main Control Panel preforms 4 communications, and on each one of them a different Sensor Unit’s values are read.

### Communications between the Main Control Panel and the Arduino Boards:
* The connection between the Arduino Boards and the Main Control Panel is a 4-wire cable with shielding, where 2 wires are used for the Board’s power supply, the two other wires are used for the A and the B RS485 protocol buses, while the shielding is used as the protocol’s ground cable.
* The main messaging protocol used is ModBus, while the only message type used is called `READ HOLDING REGISTERS`, which is called 4 times, one time four each Sensor Unit on an Arduino Board.

## Installation And Project Usage.

### Installation:
1. Required tools are Arduino IDE, an active connection to an Arduino Mega Board (also works with Uno, but the program’s pinout will need to be modified to fit the Uno Board’s pinout pattern) and an RS485 communication board.
2. First download the [main code folder](https://gitlab.com/TheMineTrooperYT/arduinoconnectionprotocol-san/-/tree/master/arduino/rs485/slave), open the folder (you will need to decend 3-4 times into the folder, until you reach a folder named “slave”).\
Inside the folder you will find a few files, one of them is called “slave.ino”.\
Open this file with the Arduino IDE.
	* _If you know how to use the CLI mode of the ARDUINO IDE, then you can use it, just make sure to use the correct programmer and CPU settings for your Board of use.\
	Example for uplaoding the slave.ino code to a Mega board: `arduino --upload slave.ino --board arduino:avr:mega:cpu=atmega256`_
3. After you opened the file with the IDE, make sure that the RS485 board is diconnected from the Arduino Board (it sits on the Tx,Rx pins of the Arduino Board, through which the IDE uploads the code, so if its connected it ‘steals’ the uploaded code, and the IDE will return a timeout error, like the Arduino Board is disconnected).
4. make changes to the SlaveID which is in the first line of the code `#define SLAVE_ID 2` to your required Slave ID.
5. On the first time you run the code on the Board, a calibration process for the Ground Moisture Sensors will take place. The process is made out of two steps, and a wait state between them and after the last step.
    1. **Step one – Dry Calibration:** On the board’s startup, the code expects the sensors to be in a dry state, and will calibrate to it. After a few moments of calibration, you need to plug in the 8th pin of the Board to the 5V pin. This will move you to the next step of the calibration.
    2. **Wait State One – Between Calibrations:** after Step One, the code waits while the 8th pin is connected for you to place the Moisture Sensors in a water container in preperation for the Wet Calibration. When the sensors are properly wet, disconnect pin 8 form the 5V pin.
    3. **Step two – Wet Calibration:** After the disconnection of pin 8, the code starts to calibrate the pins to the wet state. After a few moments of calibration connect pin 8 to stop the calibration.
    4. **Wait Step Two – End of Calibration:** now that you connected the 8th again, the calbiration process is done. Disconnect pin 8 to start the main loop of the board.
6. The Arduino Board remembers the calibration values he received on the last calibration. If you want to connect different sensors and recalibrate them, you need to clear the Board’s memeory.
    * To do so, you need to download the [clearrom code file](https://gitlab.com/TheMineTrooperYT/arduinoconnectionprotocol-san/-/tree/master/arduino/rs485/clearrom) and upload it to the Arduino Board. (remember to disconnect the RS485 chip, as explained in step 3)
7. In case of errors, you’ll need to enable the `DEBUG` flag. To do so you need to open the code file “slave.ino” in the IDE, and on the third line of code you’ll find the line `//#define DEBUG`, Remove the double-slash before that line, and reupload the code to the Arduino Board (again, remember to unplug the RS485 chip). After uploading the code, take a picture/screenshot of the output that was printed to the Serial Monitor, and send it to me on Email/Whatsapp.

### Hour Count:
* The hour count is stored inside the project page on Gitlab in [here](https://gitlab.com/TheMineTrooperYT/arduinoconnectionprotocol-san/-/issues/4), and the last line in the Issue Page is the total amount of time not reproted yet. (you can find the reported time a few lines abouve it in _italic_ font and _REPORTED_ after it.)
