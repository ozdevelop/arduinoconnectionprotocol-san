/*
  Twitter Client with Strings
 
 This sketch connects to Twitter using an Ethernet shield. It parses the XML
 returned, and looks for <text>this is a tweet</text>
 
 You can use the Arduino Ethernet shield, or the Adafruit Ethernet shield, 
 either one will work, as long as it's got a Wiznet Ethernet module on board.
 
 This example uses the DHCP routines in the Ethernet library which is part of the 
 Arduino core from version 1.0 beta 1
 
 This example uses the String library, which is part of the Arduino core from
 version 0019.  
 
 Circuit:
  * Ethernet shield attached to pins 10, 11, 12, 13
 
 created 21 May 2011
 by Tom Igoe
 
 This code is in the public domain.
 
 */
#include <SPI.h>
#include <Ethernet.h>
#include "t.h"

//serverName

//IPAddress server(51, 79, 67, 200);
char* server = "www.google.com";
const byte port = 443;

long lastAttemptTime = 0; // last time you connected to the server, in milliseconds

long requestInterval = 2000;

// initialize the library instance:
EthernetClient client;

void setup()
{
    Ethernet.init(10);

    // initialize serial:
    Serial.begin(9600);
    Serial.println(client.status());

    // morse("-.. .... .-.- .--.");non secure websites list
    byte mac[] = {0x12, 0xAA, 0xBB, 0xCC, 0xDE, 0x01};
    Ethernet.init(10);
    Ethernet.begin(mac, IPAddress(192, 168, 43, 100), IPAddress(8, 8, 8, 8), IPAddress(192, 168, 43, 0));
    // if ()
    // {
    //     morse("... ... ...");
    // }
    // else
    // {
    //     morse("..-. ..-. ..-.");
    // }
    // blink(3);
    // delay(6000);
    // blink(2, 4, 5);
    // connect to Twitter:
    connectToServer();
}

void loop()
{
    if (!Run())
    {
        Serial.println("Exit loop");
        delay(5000);
        blinkall();
        return;
    }
    if (client.connected())
    {
        if (client.available())
        {
            // read incoming bytes:
            char inChar = client.read();

            Serial.println(inChar);
        }
    }
    else if (millis() - lastAttemptTime > requestInterval)
    {
        // if you're not connected, and two minutes have passed since
        // your last connection, then attempt to connect again:
        connectToServer();
    }
}

void connectToServer()
{
    morse(".-.- --- -.");
    // attempt to connect, and wait a millisecond:
    Serial.println("connecting to server...");
    if (client.connect(server, port))
    {
        morse("... ..- .-.-");
        Serial.println("making HTTP request...");
        // make HTTP GET request to twitter:
        client.println("GET /1/statuses/user_timeline.xml?screen_name=RandyMcTester&count=1 HTTP/1.1");
        client.println("HOST: api.twitter.com");
        client.println();
    }
    else
    {
        morse("..-. .- .. .-..");
    }
    // note the time of this connect attempt:
    lastAttemptTime = millis();
}