
/*

    Copyrights ----
        Project owned by TheMineTrooper on Gitlab (https://gitlab.com/TheMineTrooperYT/arduinoconnectionprotocol-san)
        No forks or anything like that is allowed.
        No copies of the project is allowed.
        No redistribution of the project anywhere.

        If you want to use this code in your project, please contact me beforehand.
    
*/

#include "temp_sens.h"
#include "math.h"
#include <EEPROM.h>

#define BAUD 9600

#pragma region prints
#define print9(a1, a2, a3, a4, a5, a6, a7, a8, a9) \
    Serial.print(a1);                              \
    Serial.print(a2);                              \
    Serial.print(a3);                              \
    Serial.print(a4);                              \
    Serial.print(a5);                              \
    Serial.print(a6);                              \
    Serial.print(a7);                              \
    Serial.print(a8);                              \
    Serial.println(a9);                            \
    ;
#define print8(a1, a2, a3, a4, a5, a6, a7, a8) \
    Serial.print(a1);                          \
    Serial.print(a2);                          \
    Serial.print(a3);                          \
    Serial.print(a4);                          \
    Serial.print(a5);                          \
    Serial.print(a6);                          \
    Serial.print(a7);                          \
    Serial.println(a8);                        \
    ;
#define print7(a1, a2, a3, a4, a5, a6, a7) \
    Serial.print(a1);                      \
    Serial.print(a2);                      \
    Serial.print(a3);                      \
    Serial.print(a4);                      \
    Serial.print(a5);                      \
    Serial.print(a6);                      \
    Serial.println(a7);                    \
    ;
#define print6(a1, a2, a3, a4, a5, a6) \
    Serial.print(a1);                  \
    Serial.print(a2);                  \
    Serial.print(a3);                  \
    Serial.print(a4);                  \
    Serial.print(a5);                  \
    Serial.println(a6);                \
    ;
#define print5(a1, a2, a3, a4, a5) \
    Serial.print(a1);              \
    Serial.print(a2);              \
    Serial.print(a3);              \
    Serial.print(a4);              \
    Serial.println(a5);            \
    ;
#define print4(a1, a2, a3, a4) \
    Serial.print(a1);          \
    Serial.print(a2);          \
    Serial.print(a3);          \
    Serial.println(a4);        \
    ;
#define print3(a1, a2, a3) \
    Serial.print(a1);      \
    Serial.print(a2);      \
    Serial.println(a3);    \
    ;
#define print2(a1, a2)  \
    Serial.print(a1);   \
    Serial.println(a2); \
    ;
#define print1(a1)      \
    Serial.println(a1); \
    ;

#define printlist(lst, size, name)               \
    for (int __i__ = 0; __i__ < size; __i__++)   \
    {                                            \
        print5(name, '[', __i__, "]: ", lst[__i__]); \
    };
#pragma endregion
#include "modbus_slave.h"

// #ifdef MAX485
#define TxEnablePin 2 // the read/write enable pin (only on MAX485)
// #endif

#define Default_Calibration_Pin 8 // the default pin the calibration uses

// Reset function, points to start of code segment, where the reset function is defined
void (*resetFunc)(void) = 0;

#define MoistureSensorErrorThreshold 50 // the difference error thershold between dry and wet state on the moisture sensors

#define SerialMode SERIAL_8N1 // the serial mode the HardwareSerial uses

#ifndef TempSensorCount
#define TempSensorCount 3 // temperature sensor count
#endif

// if run as normal cpp and not as arduino
#ifndef ARDUINO
// define max and min
#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))
#endif
// clamps a given value into range (if above, then max, if below, then min, else value)
#define clamp(v, l, g) max(l, min(v, g))
// for shorthand
typedef unsigned int uint;

#pragma region Filefuncs

#define ROM_SIZEOF_INT 4
#define BYTE_SIZE 8
#define __flag_mark 232

namespace file_funcs
{
    // memory size of a single sensor unit data
    constexpr int unit_mem_size = 4 * ROM_SIZEOF_INT;

    /*
  puts an integer at the given index in the ROM
  @param int index: the offset in the rom
  @param int val: the value to put in the rom
*/
    void put(int index, int val)
    {
        for (int i = 0; i < ROM_SIZEOF_INT; i++) // run on int size
        {
            EEPROM.write(index + i, val & 0xFF); // write to index+offset the rightmost byte of the number
            val >>= BYTE_SIZE;                   // shift the number
        }
    }

    /*
  gets an integer from the rom at the given index
  @param int index: the offset in the rom
  @returns int: the read value
*/
    int get(int index)
    {
        int res = 0; // init ret val

        for (int i = 0; i < ROM_SIZEOF_INT; i++) // run on int size
        {
            res |= EEPROM.read(index + i) << BYTE_SIZE * i; // read into val from index+offset with shl of byte_size * offset
        }

        return res; // return
    }

    // read flag state
    bool __flagged()
    {
        return EEPROM.read(0) == __flag_mark; // if first cell is set to flag
    }

    // set flag
    void __flag(bool clear = false)
    {
        if (clear)                        // if clear
            EEPROM.write(0, 0);           // set to empty
        else                              // not clear
            EEPROM.write(0, __flag_mark); // set to flag
    }

    // returns true if the flag is set and the sensors were initalized
    bool Initialized()
    {
        return __flagged(); // return flagged state
    }

    // sets the flag on
    void set_flag()
    {
        __flag(); // set flag
    }

    // clears the flag
    void clear_flag()
    {
        __flag(true); // unset the flag
    }

    // writes a unit data at the given unit id offset.
    void WriteUnitData(int unit_id, int max1, int min1, int max2, int min2)
    {
        unit_id = unit_id * unit_mem_size + 1; // apply offset
        print1(unit_id);
        put(unit_id, max1);                      // write at offset
        put(unit_id + ROM_SIZEOF_INT, min1);     // write at offset + ROM int offset
        put(unit_id + ROM_SIZEOF_INT * 2, max2); //.. *2
        put(unit_id + ROM_SIZEOF_INT * 3, min2); //.. *3
    }

    void ReadUnitData(int unit_id, int *max1, int *min1, int *max2, int *min2)
    {
        unit_id = unit_id * unit_mem_size + 1;     // apply offset
        *max1 = get(unit_id);                      // read at offset
        *min1 = get(unit_id + ROM_SIZEOF_INT);     // read at offset + ROM int offset * 1
        *max2 = get(unit_id + ROM_SIZEOF_INT * 2); //.. *2
        *min2 = get(unit_id + ROM_SIZEOF_INT * 3); //.. *3
    }
} // namespace file_funcs

#pragma endregion

#pragma region SensorUnit

// represents a sensor unit (TempSensorCount temperature, 2 conductivity, 1 light)
class SensorUnit
{
private:
// all the sensor variables
#pragma region Sensors

    SensorInterface<TempSensorCount> *sens; // the temperature sensor interface pointer

    uint Moisture_Sens_1_Pin; // the pin of the first conductivity sensor

    uint Moisture_Sens_2_Pin; // the pin of the sencond conductivity sensor

    uint Light_Sens_Pin; // the pin of the light sensor

#pragma region multipliers

    float Temperautre_Multiplier; // the mutliplier for the temperature sensors

    // the ranges of the analog sensors, used for mapping into a 0-100 range
    enum SensorRanges
    {
        LightMin = 0,    // light min value
        LightMax = 1024, // light max value
    };

#pragma endregion

    // sets the pins to output
    // @param uint ow_pin: the pin the OneWire is connected to
    void SetPins();

#pragma endregion

#pragma region Regs

    uint *Regs; // a pointer to the register array

    uint unit_offset; // the offset of the current unit within the register array

    // holds data about the offsets of the different sensors in the register array
    enum offsets
    {
        Moisture1, // moisture sensor
        Moisture2, // moisture sensor
        Light,     // light sensor

        Temper1, // temperature sensors (the first sensor, later calculated using the given sensor count)

        SensorCount // total count (not including all temperature sensors, calculated later)
    };

#pragma endregion

#pragma region MoistureSensorCalibration

    int moisture_sensor_calibration_pin;

    int Moisture1Wet = 0;    // moisture wet value
    int Moisture1Dry = 1024; // moisture dry value
    int Moisture2Wet = 0;    // moisture wet value
    int Moisture2Dry = 1024; // moisture dry value

    bool calibrated_sensors = false;

#pragma endregion

public:
    // The Default address space for a single unit.
    static constexpr uint DefaultAddressSpace = TempSensorCount + 2 + 1; // N temperature + 2 conduct + 1 light

    /*
      Constructor:
      constructs all the different variables, pins and pointers

      @param uint OneWire_pin:                            the pin the OneWire sits on
      @param uint moisture_sens_1_Pin:                    the pin the first conductivity sensor sits on
      @param uint moisture_sens_2_Pin:                    the pin the second conductivity sensor sits on
      @param uint light_sens_pin:                         the pin the light sensor sits on
      @param uint* regs:                                  the registor array
      @param SensorInterface<TempSensorCount>* si:        the temperature sensor interface pointer
      @param uint offset = 0:                             the unit offset of this unit in the register array (defaults to 0 [no offset])
      @param float temper_multiplier = 1:                 the temperature multiplier (defaults to 1 [no multipler])
      @param int moisture_sensor_dry_calibration = def:   the moisture sensor calibration pin (if high, then calibrating)
    */
    SensorUnit(uint moisture_sens_1_Pin,
               uint moisture_sens_2_Pin,
               uint light_sens_pin,
               uint *regs,
               SensorInterface<TempSensorCount> *si,
               uint offset = 0,
               float temper_multiplier = 1);

    /*
      Destructor:

      Called upon object deletion and clears the allocated storage within the object.

      deletes the temperature sensor address array.
    */
    ~SensorUnit();

    /*
      Reads the values from the different sensors and writes them to the regsiter array
    */
    void SetSensorReadings();

    /*
      returns the unit offset of the object

      @return uint: the offset of the object
    */
    uint GetUnitOffset() const;

    /*
      setsthe unit offset to the given value

      @param uint offset: the offset to use
    */
    void SetUnitOffset(const uint offset);

    /*
      sets the regsiter array

      @param uint* regs: the new pointer
    */
    void SetRegisterPointer(uint *regs);

    // calibrates the moisture sensors at dry state
    void CalibrateMoistureDryState();
    // calibrates the moisture sensors at wet state
    void CalibrateMoistureWetState();

    /*
      write the unit data to the rom at given unit offset
      @param int unit_index: the unit offset
    */
    void WriteUnitData(int unit_index);

    /*
      read the unit data from the rom at given unit offset
      @param int unit_index: the unit offset
    */
    void ReadUnitData(int unit_index);

    byte CheckMoisutreSensor();

    // just print
    void print_unit_data(int unit_id)
    {
        print9(unit_id, "max1: ", Moisture1Dry, ", min1: ", Moisture1Wet, ", max2: ", Moisture2Dry, ", min2:", Moisture2Wet);
    }
};

#pragma endregion

#pragma region SensorUnit class funcs

#define unit_templ(type) type SensorUnit::

unit_templ() SensorUnit(uint moisture_sens_1_Pin,
                        uint moisture_sens_2_Pin,
                        uint light_sens_pin,
                        uint *regs,
                        SensorInterface<TempSensorCount> *si,
                        uint offset,
                        float temper_multiplier) : Moisture_Sens_1_Pin(moisture_sens_1_Pin), // set the conductivity pin
                                                   Moisture_Sens_2_Pin(moisture_sens_2_Pin), // set the sencond conductivity pin
                                                   Light_Sens_Pin(light_sens_pin),           // set the light pin
                                                   Regs(regs),                               // set the register array
                                                   unit_offset(offset),                      // set the unit offset
                                                   Temperautre_Multiplier(temper_multiplier) // set the temperature multipler
{
    // printf("moist1:%d,moist2:%d,light:%d\n", moisture_sens_1_Pin, moisture_sens_2_Pin, light_sens_pin);
    sens = si; // save pointer
    SetPins(); // set the pins
}

unit_templ() ~SensorUnit()
{
    delete sens; // delete the assigned pointer of the sensor interface
}

unit_templ(void) SetSensorReadings()
{

    // Reads the different analog sensors and maps them to a 0-100 range
#if defined(ARDUINO) && !defined(NO_SENS)
    long moisture1 = map(analogRead(Moisture_Sens_1_Pin), Moisture1Wet, Moisture1Dry, 100, 0); // read conductivity sensor 1 with multipler
    long moisture2 = map(analogRead(Moisture_Sens_2_Pin), Moisture2Wet, Moisture2Dry, 100, 0); // read conductivity sensor 2 with multipler

    moisture1 = clamp(moisture1, 0, 100); // clamp to 0-100 range
    moisture2 = clamp(moisture2, 0, 100); //

    long light = map(analogRead(Light_Sens_Pin), SensorRanges::LightMax, SensorRanges::LightMin, 100, 0); // read light sensor with multipler

#else
    long moisture1 = clamp(Moisture_Sens_1_Pin++, 0, 55555);
    if (Moisture_Sens_1_Pin > 55555)
        Moisture_Sens_1_Pin = 0;
    long moisture2 = 2;
    long light = 3;
#endif

    float *temps = sens->GetTemperatures(); // get the temperatures from the SensorInterface

    // Serial.print("moisture1 ");
    // Serial.println(moisture1);
    // // delay(500);
    // Serial.print("moisture2 ");
    // Serial.println(moisture2);
    // // delay(500);
    // Serial.print("light ");
    // Serial.println(light);
    // // delay(500);
    // for (int i = 0; i < TempSensorCount; i++)
    // {
    //     Serial.print("temp ");
    //     Serial.print(i);
    //     Serial.print(": ");
    //     Serial.println(temps[i]);
    // }
    //
    // delay(5000);
    // return;

    // set regsisters in place of unit offset + sensor offset to sensor reading:
    Regs[unit_offset + offsets::Moisture1] = moisture1; // conductivity 1
    Regs[unit_offset + offsets::Moisture2] = moisture2; // conductivity 1
    Regs[unit_offset + offsets::Light] = light;         // light

    for (int i = 0; i < TempSensorCount; i++) // run on temperature sensor count
    {
        Regs[unit_offset + offsets::Temper1 + i] = Temperautre_Multiplier * max(temps[i], 0); // set registers in place of unit offset +
                                                                                              // temp sensor 1 + index to reading at index,
                                                                                              // max the reading with 0 (set to above 0) and apply multiplier
    }

    // Serial.print("moisture1 ");
    // Serial.println(Regs[unit_offset + offsets::Moisture1]);
    // delay(500);
    // Serial.print("moisture2 ");
    // Serial.println(Regs[unit_offset + offsets::Moisture2]);
    //
    // delay(500);
    // Serial.print("light ");
    // Serial.println(Regs[unit_offset + offsets::Light]);
    // delay(500);
    // for (int i = 0; i < TempSensorCount; i++)
    // {
    //     Serial.print("temp ");
    //     Serial.print(i);
    //     Serial.print(": ");
    //     Serial.println(Regs[unit_offset + offsets::Temper1 + i]);
    // }

    // no clue why, we need this, otherwise first reg gets a zero.
    delay(100);
}

unit_templ(uint) GetUnitOffset() const
{
    return unit_offset; //return the offset
}

unit_templ(void) SetUnitOffset(const uint offset)
{
    unit_offset = offset; // set the offset
}

unit_templ(void) SetRegisterPointer(uint *regs)
{
    Regs = regs; // set the pointer field
}

unit_templ(void) SetPins()
{
#if defined(ARDUINO) && !defined(NO_SENS)
    pinMode(Light_Sens_Pin, INPUT);                  // set light
    pinMode(Moisture_Sens_1_Pin, INPUT);             // set moisture1
    pinMode(Moisture_Sens_2_Pin, INPUT);             // set moisture2
    pinMode(moisture_sensor_calibration_pin, INPUT); // set the calibration pin
#endif
}

// unit_templ(void) CalibrateMoistureSensors()
// {
//     if (calibrated_sensors) // if calibrated
//     {
//         print1("Calibrated Moisture Sensors Already."); // print and exit
//         return;
//     }
//
//     // inform user
//     print7("Calibrating Moisture Sensors On Pins ", Moisture_Sens_1_Pin, ", ", Moisture_Sens_2_Pin, " To Dry State. Plug Pin ", moisture_sensor_calibration_pin, " When Calibrated (give it a second or two).");
//     delay(2000);
//     while (!digitalRead(moisture_sensor_calibration_pin)) // as long as pin is unplugged
//     {
//         Moisture1Dry = analogRead(Moisture_Sens_1_Pin);
//         Moisture2Dry = analogRead(Moisture_Sens_2_Pin); // calibrate dry state
//         // print1("Calibrated Sensors To Dry State.");     // print
//         delay(500); // wait
//     }
//     print1("Done Calibrating Dry State"); // inform on finished calibration step
//
//     // inform user to pull it off
//     print1("Wet the Sensors Now.\n\rPlease Unplug The Powered Pin (The one you just connected) And Wait For a Moment After You Wetted The Sensors.");
//     while (digitalRead(moisture_sensor_calibration_pin)) // as long as the pin is still powered
//     {
//         delay(500);
//     }
//
//     // inform user
//     print7("Calibrating Moisture Sensors On Pins ", Moisture_Sens_1_Pin, ", ", Moisture_Sens_2_Pin, " To Wet State. Plug Pin ", moisture_sensor_calibration_pin, " When Calibrated (give it a few senconds).");
//     while (!digitalRead(moisture_sensor_calibration_pin)) // as long as pin is unplugged
//     {
//         Moisture1Wet = analogRead(Moisture_Sens_1_Pin);
//         Moisture2Wet = analogRead(Moisture_Sens_2_Pin); // calibrate wet state
//         // print1("Calibrated Sensors To Wet State.");     // print
//         delay(500); // wait
//     }
//     print1("Done Calibrating Wet State"); // inform on finished calibration step
//
//     // inform user to pull it off
//     print1("Please Unplug The Powered Pin (The one you just connected) And Wait For a Moment.");
//     while (digitalRead(moisture_sensor_calibration_pin)) // as long as the pin is still powered
//     {
//         delay(100);
//     }
//
//     calibrated_sensors = true;
//
//     // inform user on Done Calibrating
//     print1("Finished  Calibration Of Moisture Sensors.");
//     delay(100); // wait
// }

unit_templ(void) CalibrateMoistureWetState()
{
    Moisture1Wet = analogRead(Moisture_Sens_1_Pin);
    Moisture2Wet = analogRead(Moisture_Sens_2_Pin); // calibrate wet state
}

unit_templ(void) CalibrateMoistureDryState()
{
    Moisture1Dry = analogRead(Moisture_Sens_1_Pin);
    Moisture2Dry = analogRead(Moisture_Sens_2_Pin); // calibrate dry state
}

unit_templ(void) WriteUnitData(int unit_index)
{
    file_funcs::WriteUnitData(unit_index, Moisture1Dry, Moisture1Wet, Moisture2Dry, Moisture2Wet); // write fields
}

unit_templ(void) ReadUnitData(int unit_index)
{
    file_funcs::ReadUnitData(unit_index, &Moisture1Dry, &Moisture1Wet, &Moisture2Dry, &Moisture2Wet); // read fields
}

unit_templ(byte) CheckMoisutreSensor()
{
    byte ret = 0; // set flag var

    bool s1 = abs(Moisture1Dry - Moisture1Wet) <= MoistureSensorErrorThreshold; // get absolute difference, if bellow threshold, set error bit
    bool s2 = abs(Moisture2Dry - Moisture2Wet) <= MoistureSensorErrorThreshold; // get absolute difference, if bellow threshold, set error bit

#ifdef DEBUG
    print4("Sensors state. Sensor 1 status is ", s1 ? "bugged" : "fine", ", Sensor 2 status is ", s1 ? "bugged" : "fine");
#endif

    if (s1)          // if sensor 1 is bugged
        ret |= 0b01; // light first bit
    if (s2)          // if sensor 2 is bugged
        ret |= 0b10; // light second bit

    return ret; // return flag var
}

#pragma endregion

#pragma region Units

// if didnt define override value for unit count
#ifndef _count
#define _count 4
#endif
// set formal name
#define UnitCount _count
// #undef _count

/*
  defiens a group of pin connections

  @param moisture1: the first moisture sensor pin
  @param moisture2: the second moisture sensor pin
  @param light: the light sensor pin

  @param temps[]: an array of OneWire pins for the temperature sensors
*/
struct UnitPins
{
public:
    int moisture1; // the first moisture sensor
    int moisture2; // the sencond moisture sensor
    int light;     // the light sensor

    int temps[TempSensorCount]; // an array of the OneWire pins for the temperature sensors
};

/*
  defiens a list of Sensor units and handles all of the modbus communications
*/
templ class Units
{
private:
    SensorUnit *unit_arr[N]; // an array of the different SensorUnits

    static const uint HOLDING_REGS_COUNT = UnitCount * SensorUnit::DefaultAddressSpace + 1; // the amount of Holding Registers in the memory (predefined)
    uint holdingRegs[HOLDING_REGS_COUNT]{0};                                                // function 3 and 16 register array

public:
    /*
      Inits a Units object.

      @param int id: the slave id.
      @param UnitPins[N] pins_data: an array of the different pin data objects
      @param int baud = BAUD (9600): the BAUD rate of the RS485 connection
      @param int serial_mode = SerialMode (SERIAL_8N2): the connection type of the serial
    */
    Units(int id, UnitPins pins_data[N], int baud = BAUD, int serial_mode = SerialMode);

    // called throught the loop of the arduino, handles the sensor readout and the modbus update.
    void loop();

    // prints the registers array
    void printRegs();

    // calibrates the moisture sensors
    void Calibrate_Moisture_Sensors();
};

// shortcut
#define units_templ(type) templ type Units<N>::

units_templ() Units(int id, UnitPins pins_data[N], int baud, int serial_mode)
{
    // configure the modbus connection
    modbus_configure(&Serial,
                     baud, serial_mode, id,
    // if not using MAX485 (Groove RS485) then disable TxEnablePin
#ifdef MAX485
                     TxEnablePin,
#endif
                     HOLDING_REGS_COUNT, holdingRegs);

    // set first register to the slave id in order to check connection
    holdingRegs[0] = id;

    for (int i = 0; i < N; i++) // run on unit count
    {
        // create unit
        unit_arr[i] = new SensorUnit(pins_data[i].moisture1, // read pin-out data from pin object
                                     pins_data[i].moisture2,
                                     pins_data[i].light,
                                     holdingRegs, // the register pointer
                                     new SensorInterface<TempSensorCount>(
                                         pins_data[i].temps),                 // create sensor interface in MultiPin mode
                                     i * SensorUnit::DefaultAddressSpace + 1, // set offset to index multipler * default address space + 1 offset for first index
                                     100);                                    // set temperature multipler
                                                                              // print2("Calibrating Sensor Unit ", i);
    }

    Calibrate_Moisture_Sensors();

#ifdef DEBUG
    print1("Finished Initalization of Sensor Units.");
#endif
}

units_templ(void) loop()
{
    // waits for msg on the serial bus
    auto wait_for_msg = [this](unsigned long timeout) {
        unsigned long mil = millis();                                     // get inital time
        unsigned long newt = 0;                                           // init counter
        while (!Serial.available() && (newt = millis()) - mil <= timeout) // run as long as no msg and not timeout
        {
        }
        return newt - mil < timeout; // return difference bellow timeout threshold
    };

    for (int i = 0; i < 4; i++) // run on unit & msg count
    {
        if (!wait_for_msg(5000UL)) // wait for msg, if failed then timeout
        {
#ifdef DEBUG
            print1("timeout"); // print timeout
#endif
            delay(100);  // wait (to allow print to finish)
            resetFunc(); // reset the arduino
        }

        for (int i = 0; i < N; i++) // run on unit count
        {
            unit_arr[i]->SetSensorReadings(); // read each unit sensors into the registry array
        }

        modbus_update(); // update the modbus

#ifdef DEBUG
        printlist(frame, buffer, "Frame");
#endif

        if (!GotCorrectId()) // if incorrect id (not for me)
        {
#if defined(DEBUG)
            print5("Got incorrect id (id=", GetId(), ",my_id=", slaveID, "), exiting");
#endif
            delay(100);
            resetFunc(); // reset
        }
    }

#ifdef DEBUG
    delay(SLEEP_DELAY - 100);
    print1("reset");
    delay(100);
#else
    delay(SLEEP_DELAY);
#endif
    resetFunc();
}

units_templ(void) Calibrate_Moisture_Sensors()
{
    if (file_funcs::Initialized()) // if initialized
    {
        for (int i = 0; i < N; i++) // run on unit count
        {
            unit_arr[i]->ReadUnitData(i); // read unit
#ifdef DEBUG
            unit_arr[i]->print_unit_data(i); // print data
#endif
        }
        return; // exit
    }
    // dry loop
    print3("Calibrating Dry State. Connect pin ", Default_Calibration_Pin, " When done."); // print dry info
    while (!digitalRead(Default_Calibration_Pin))                                          // while calibration pin is not connected
    {
        for (int i = 0; i < N; i++) // run on unit count
        {
            unit_arr[i]->CalibrateMoistureDryState(); // calibrate dry
        }
    }
    print3("Unplug pin ", Default_Calibration_Pin, " when the sensors are wet.");
    while (digitalRead(Default_Calibration_Pin))
    {
        delay(100);
    }
    print3("calibrating Wet State. Connect pin ", Default_Calibration_Pin, " When done.");
    while (!digitalRead(Default_Calibration_Pin))
    {
        for (int i = 0; i < N; i++)
        {
            unit_arr[i]->CalibrateMoistureWetState();
        }
    }
    print3("Unplug pin", Default_Calibration_Pin, ", Calibration is Done.");
    while (digitalRead(Default_Calibration_Pin))
    {
        delay(100);
    }

    for (int i = 0; i < N; i++) // run on unit count
    {
        byte stat = unit_arr[i]->CheckMoisutreSensor(); // set sensor status

        // if (stat != 0)
        // {
        print6("Sensor Unit At Index ", i, ": \n        - Sensor 1 status is ", stat & 0b01 == 1 ? "bugged" : "fine", ".\n       - Sensor 2 status is ", stat & 0b10 == 1 ? "bugged." : "fine.");
        // }

        unit_arr[i]->WriteUnitData(i); // save the calibration data to the ROM
    }

    file_funcs::set_flag(); // set init flag
}

units_templ(void) printRegs()
{
    print4("unit count: ", N, ", reg count (N*6+1):", HOLDING_REGS_COUNT);
    for (int i = 0; i < HOLDING_REGS_COUNT; i++) // run on register count
    {
        // if not in arduino
#ifndef ARDUINO
        printf("reg %d: %d\n", i, holdingRegs[i]); // use printf
                                                   // if on arduino
#else
        // Serial.print("reg "); // print using Serial
        // Serial.print(i);
        // Serial.print(": ");
        // Serial.println(holdingRegs[i]);
        print4("reg ", i, ": ", holdingRegs[i]); // print reg at i
#endif
    }
    // same ^^
#ifndef ARDUINO
    printf("\n");
#else
    Serial.println();
#endif
}

#pragma endregion

// #undef print7
// #undef print6
// #undef print5
// #undef print4
// #undef print3
// #undef print2
// #undef print1
