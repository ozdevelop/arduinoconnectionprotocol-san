#define SLAVE_ID 2

//#define DEBUG

/*

    Copyrights ----
        Project owned by TheMineTrooper on Gitlab (https://gitlab.com/TheMineTrooperYT/arduinoconnectionprotocol-san)
        No forks or anything like that is allowed.
        No copies of the project is allowed.
        No redistribution of the project anywhere.

        If you want to use this code in your project, please contact me beforehand.
    
*/

// use this to override default Unit Count
#define _count 4
// // use this to override default Temperature Sensor Count
// #define tempcount 3
// // define this if on ARDUINO and not C++
#define ARDUINO
// // define this if using MAX485 rs485 chip (comment out if using the new chip)
// #define MAX485

#define NO_SENS

// #define DEBUG

// // the BAUD rate (you can ommit this, its the default value)
#define BAUD 9600

#define SLEEP_DELAY 5000

#include "SensorUnit.h"

// // init units object
Units<UnitCount> *units;

// constexpr unsigned int reg_count = 6;

// unsigned int _regs[reg_count]{55, 55, 55, 55, 55, 55};

// SensorUnit unit(A0, A1, A2, _regs, new SensorInterface<3>(new int[3]{22, 23, 24}), 0, 1);

void setup()
{

    // create the object
    units = new Units<UnitCount>(
        // The Slave ID:
        SLAVE_ID,
        // The Sensor Pin Connections:
        // (moisture1 is first moisture sensor analog pin connection, moisture2 is the second moisture sensor analog pin connection, light is the light sensor analog pin connection,
        //      temps is a list of the digital pins the temperature sensors are connected to (like this: {22, 23, 24}) )
        new UnitPins[UnitCount]{
            {.moisture1 = A0, .moisture2 = A1, .light = A2, .temps = {22, 23, 24}},
            {.moisture1 = A3, .moisture2 = A4, .light = A5, .temps = {25, 26, 27}},
            {.moisture1 = A6, .moisture2 = A7, .light = A8, .temps = {28, 29, 30}},
            {.moisture1 = A9, .moisture2 = A10, .light = A11, .temps = {31, 32, 33}}},
        // you can remove this parameter if we're using 9600, that's the default value.
        BAUD, SerialMode);

    // modbus_configure(&Serial, 9600, SERIAL_8N2, 1, 2, reg_count, _regs);
    // int max1, max2, min1, min2;
    // file_funcs::ReadUnitData(1, &max1, &min1, &max2, &min2);

    // print7(max1, ',', min1, ',', max2, ',', min1);
}

void loop()
{
    // call loop func, sets the sensor readings and triggers modbus readout
    units->loop();

    // modbus_update();
    // unit.SetSensorReadings();
}
