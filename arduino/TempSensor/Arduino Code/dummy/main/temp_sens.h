/*

    Copyrights ----
        Project owned by TheMineTrooper on Gitlab (https://gitlab.com/TheMineTrooperYT/arduinoconnectionprotocol-san)
        No forks or anything like that is allowed.
        No copies of the project is allowed.
        No redistribution of the project anywhere.

        If you want to use this code in your project, please contact me beforehand.

*/

#pragma once
#define ARDUINO 100
#include <OneWire.h>
#include <DallasTemperature.h>

#define addrLen 8

#define maxSensorCount 64

#ifndef tempcount
#define TempSensorCount 6 // temperature sensor count
#else
#define TempSensorCount tempcount
#endif

#define byte uint8_t

#define err_temp -127.0

#define templ template <byte N>

#define arrLength(arr) sizeof(arr) / sizeof(arr[0])

#pragma region Pair definition

// Stores a pair of values of different types
template <typename T1, typename T2>
class Pair
{
public:
    // default constructor
    // @param T1: value to save at Value1
    // @param T2: value to save at Value2
    Pair(T1, T2);
    // copy constructor
    // @param Pair&: the other pair to copy from
    Pair(const Pair &);

    // The first saved value
    T1 Value1;

    // the second saved value
    T2 Value2;
};

template <typename T1, typename T2>
Pair<T1, T2>::Pair(T1 v1, T2 v2) : Value1(v1), Value2(v2) {}
template <typename T1, typename T2>
Pair<T1, T2>::Pair(const Pair &other) : Pair(other.Value1, other.Value1) {}

#pragma endregion

class SensorInterface
{
private:
#pragma region single port multiple sensors
    /// Vector of addresses
    DeviceAddress *addrArr;
#if defined(ARDUINO) && !defined(NO_SENS)
    /// OneWire interface object
    OneWire ow;
    /// sensor manager interface object
    DallasTemperature *sensors;
#endif

    /// @brief initalizes the Address array (same as DallasTemperature.begin)
    void initAddresses();

    /// @brief copies one address into another
    /// @param src source address
    /// @param dest target address
    void CopyAddress(const DeviceAddress src, DeviceAddress &dest);

#pragma endregion
public:
    // @param int: the pin the OneWire connection sits on.
    // @param bool=false: if true, prints all the addresses of the connected temperature sensors
    SensorInterface(int, bool = false);

    /*
    dtor
    */
    ~SensorInterface();

    // @return an array of temperatures read from the sensors
    float *GetTemperatures();

    /*
    @return an array of Pair pointers.
        - each pair holds a byte array as the address (first value) and a float as the returned degree (second value)
    */
    Pair<byte *, float> **GetTemperaturesWithAddressess();

    // @return the temperature on the given address
    // @param byte[addrLen] the byte address of the sensor to check on
    float GetTemperatureOnAddress(DeviceAddress);

    /// sensor count
    int sensor_count;

    // @return true if the given address is empty
    // @param byte* the address to check for validity
    static bool isAddrEmpty(DeviceAddress);
};

// constructors and destructor
#pragma region ctor + dtor

SensorInterface::SensorInterface(int oneWire_pin, bool getAddrs)
{
#if defined(ARDUINO) && !defined(NO_SENS)

    ow = OneWire(oneWire_pin);            // onewire ctor
    sensors = new DallasTemperature(&ow); // temp sensor ctor
    initAddresses();                      // init addresses
    sensors->begin();                     // init dallasTemperature bus

    sensors->setResolution(9); // set reosultion of all the sensors

    if (getAddrs) // if the print flag is lit
    {
        Serial.println("addresses:");
        for (int i = 0; i < sensor_count; i++) // run on sensors
        {
            Serial.print("{ ");               // start address print
            for (int j = 0; j < addrLen; j++) // run on address
            {
                Serial.print("0x");               // print prefix
                Serial.print(addrArr[i][j], HEX); // print hex data
                if (j + 1 < addrLen)              // if before the end
                    Serial.print(", ");           // print spacer
                else                              // else (at last item)
                    Serial.print(" }");           // print end of address
            }
            Serial.println(); // newline
        }
        Serial.println("waiting 3 seconds (for ease of copying the address/es)");
        delay(3000); // wait 3 seconds
    }

#endif
}

SensorInterface::~SensorInterface()
{
#if defined(ARDUINO) && !defined(NO_SENS)
    if (addrArr != NULL)
    {
        for (int i = 0; i < sensor_count; i++) // run on sensor count
        {
            delete[] addrArr[i]; // delete address in array
        }
        delete[] addrArr; // delete array
    }
    if (sensors != NULL)
    {
        delete sensors;
    }
#endif
}

#pragma endregion

void SensorInterface::initAddresses()
{
    ow.reset_search(); // reset bus search

    DeviceAddress deviceAddress;                 // store found address
    DeviceAddress tempAddresses[maxSensorCount]; // temperary address array
    int deviceCount = 0;                         // device counter
    while (ow.search(deviceAddress))             // as long as another device is found
    {
        if (sensors->validAddress(deviceAddress)) // if address is valid
        {
            deviceCount++; // add device

            if (deviceCount > maxSensorCount) // limit sensor count
                break;
            CopyAddress(deviceAddress, tempAddresses[deviceCount - 1]); // copy address to array
        }
    }

    if (deviceCount == 0) // if no devices were found
        return;

    addrArr = new DeviceAddress[deviceCount]; // init address array to device count
    for (int i = 0; i < deviceCount; i++)     // iterate over devices
    {
        CopyAddress(tempAddresses[i], addrArr[i]); // copy from temp array to address array
    }

    sensor_count = deviceCount; // store device count
}

void SensorInterface::CopyAddress(const DeviceAddress src, DeviceAddress &dest)
{
    for (int i = 0; i < addrLen; i++)
    {
        dest[i] = src[i];
    }
}

float *SensorInterface::GetTemperatures()
{
    float *ret = new float[this->sensor_count]{err_temp}; // init return array
#if defined(ARDUINO) && !defined(NO_SENS)

    sensors->requestTemperatures(); // request temperatures from the sensor interface

    for (int i = 0; i < this->sensor_count; i++) // run on sensor count
    {
        byte *iter = addrArr[i]; // get address

        if (isAddrEmpty(iter))            // if empty
            continue;                     // skip
        ret[i] = sensors->getTempC(iter); // get temperature on address into array[i]
        // Serial.print(i);
        // Serial.print(": ");
        // Serial.println(ret[i]);
    }

#else
    for (int i = 0; i < this->sensor_count; i++)
    {
        ret[i] = 0;
    }

#endif

    return ret; // return array
}

Pair<byte *, float> **
SensorInterface::GetTemperaturesWithAddressess()
{
    Pair<byte *, float> **ret_arr = new Pair<byte *, float> *[this->sensor_count + 1]; // init array of pairs to return (1 larger than needed)
#if defined(ARDUINO) && !defined(NO_SENS)

    sensors->requestTemperatures();          // request temperatures
    int i = 0;                               //
    for (i = 0; i < this->sensor_count; i++) // run on sensor count
    {
        ret_arr[i] = new Pair<byte *, float>(addrArr[i], sensors->getTempC(addrArr[i])); // set array in place of i to
                                                                                         // new pair of address at i and
                                                                                         // temperature of sensor at given address
    }

#endif
    ret_arr[this->sensor_count] = NULL; // set last element to null

    return ret_arr; // return
}

float SensorInterface::GetTemperatureOnAddress(DeviceAddress address)
{
    float temp = err_temp; // temperature reading
#if defined(ARDUINO) && !defined(NO_SENS)

    // checks if address exists on the device (and was loaded)
#pragma region check address

    bool success = false; // flag
    int addr_index = 0;
    for (int i = 0; i < sensor_count; i++) // run on sensor count
    {
        bool fail = false;                // set fail flag
        for (int j = 0; j < addrLen; j++) // run on address
        {
            if (addrArr[i][j] != address[j]) // if not the same address as in the address array
            {
                fail = true; // set fail flag
                break;       // exit loop
            }
        }
        if (!fail) // if didnt fail
        {
            success = true; // set flag
            addr_index = i;
            break; // exit loop
        }
    }
    if (!success) // if failed
    {
        return err_temp; // return error temperature
    }
#pragma endregion

    byte *l = new byte[addrLen];
    for (int i = 0; i < addrLen; i++)
    {
        l[i] = address[i];
    }

    sensors->requestTemperatures(); // request temperatures
    temp = sensors->getTempC(l);    // get temperature at address

#elif defined(ARDUINO)
    temp = 0;
#endif
    return temp; // ret
}

bool SensorInterface::isAddrEmpty(DeviceAddress addr)
{
    if (addr == nullptr) // if pointer is null
    {
        return true; // empty
    }

    for (int i = 0; i < arrLength(addr); i++) // run on address length
    {
        if (addr[i] != 0) // if slot is not empty
            return false; // addr isnt empty
    }
    return true; // all slots are empty
}