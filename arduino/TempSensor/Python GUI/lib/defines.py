
# defines for the main code

EOF = -1
EOF_BYTES = bytes([0xFF])

FONT_SMALL = ("Ariel", 10)
FONT = ("Ariel", 11)
FONT_LARGE = ("Ariel", 12)

HANDSHAKE_ATTEMPTS = 10
""" how many handshake attempts should be allowed """

TIMEOUT_HANDSHAKE = 1  # seconds
""" timeout for handshake attempts """

TIMEOUT_DEFAULT = None
""" default timeout """

TIMEOUT_THREAD = 5
""" timeout in Sensor Read Thread """

SAVE_FILE = "arduinoInterfaceSave.save"
""" path of save file for IDs """

SENSOR_UPDATE_RATE = 0.5  # seconds
""" delay between sensor updates """

CLOSE_DELAY_RETRY = 20  # tries
""" how many times to wait for sensorReadThread to finish before exiting"""

KQ_ACCURACY = 3
""" how many decimal places to keep """

# APP_CODE_ = ""
APP_CODE_INIT_HANDSHAKE = "INITHANDSHAKE"
APP_CODE_SENSOR_DISTANCE_SET = "SETUPCOMPLETE"
APP_CODE_SENSOR_IDS_SET = "POSITIONSSET"
