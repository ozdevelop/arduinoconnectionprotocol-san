from lib.imports import *
from lib.defines import *
from lib.utils import *
from lib.tkWindows import *


class App(tk.Tk):

    serialPort = ""  # arduino's serial port

    lockedSensorData: LockedVariable = None

    sensorReadDT: LockedVariable = None

    window: ttk.Frame = None  # the current active window

    def __init__(self):
        super().__init__()
        self.title("Temperature Sensor Pack")  # set title
        self.resizable(False, False)  # set not resizable
        self.protocol("WM_DELETE_WINDOW", self.on_closing)  # link close event

        self.lockedSensorData = LockedVariable([])
        self.sensorReadDT = LockedVariable(0.0)

        self.window = FindComPort(self)  # set and pack findComPort window
        self.window.pack()

    def on_closing(self):
        """ called when the window is closed """
        if messagebox.askokcancel("Quit", "Do you want to quit?\nThis will also close the Sensor Read Thread, which will take some time.\nIt also sometimes gets stuck, in which case just ignore it, it'll close after some time."):  # ask for confirmation
            if not hasattr(self, "sensorReadThread"):
                self.window.destroy()  # close active window
                self.destroy()
                return

            with self.serPort.getLock("onclose") as sp:
                sp.write(ConnectionModes.wait.value)

            self.stopReadThread = True  # set stop thread flag
            count = 0
            # print("Stopping")
            while self.sensorReadThread.is_alive():  # wait for thread to stop
                # print("wiating for thread to stop", count)
                time.sleep(SENSOR_UPDATE_RATE)  # wait sensor update rate
                count += 1
                if count >= CLOSE_DELAY_RETRY:
                    break
            # self.window.destroy()  # close active window
            self.destroy()  # destroy self

    def message(self, message: str, *args):
        if message == APP_CODE_INIT_HANDSHAKE:
            self.__init_handshake(*args)
        elif message == APP_CODE_SENSOR_DISTANCE_SET:
            self.__sensor_distance_set(*args)
        elif message == APP_CODE_SENSOR_IDS_SET:
            self.__sensor_ids_set(*args)

    def __init_handshake(self):
        self.window.destroy()  # destroy the findComPort window

        # set label & title
        self.loading_text_base = f"Initializing handshake with Arduino\nboard on the selected port ({self.serialPort})..."
        self.loading_text = tk.StringVar(
            self, self.loading_text_base)
        self.window = tk.Label(
            self, textvariable=self.loading_text, font=FONT_LARGE)
        self.window.pack(padx=25, pady=25, fill='both')
        self.title("Connecting...")

        self.hsThread = threading.Thread(
            target=self.handshakeThread)  # create handshake thread
        self.hsThread.start()  # start it

    def __sensor_ids_set(self, ids):
        self.idMap = ids
        self.window.destroy()

        self.window = SensorDistances(self, self.sensorCount)
        self.window.pack()

    def __sensor_distance_set(self, poss):
        self.sensorPositions = poss
        self.window.destroy()
        # print(poss)
        # self.window = Sensor3DGraph(self, self.idMap, poss)
        self.title("Sensor Readouts")
        self.window = SensorGraphs(self, self.idMap, poss)
        self.window.pack()
        # self.window = SensorGraphs(self, ids)
        # self.window.pack(side='top', fill='both', expand=True)

    """ Threads """

    def handshakeThread(self):
        """ deals with the board handshake """
        self.serPort = LockedVariable(connectToBus(
            self.serialPort))  # connect to board, store in a locked variable

        time.sleep(3)  # wait for board to finish restarting

        with self.serPort.getLock("handshakeThread") as sp:  # acquire lock
            sp: serial.Serial = sp

            # print("sending code")

            i = sp.write(ConnectionModes.setup.value)  # send setup code

            # print("sent code")
            time.sleep(3)  # wait for board to receive and send

            data = None  # read data

            sp.timeout = TIMEOUT_HANDSHAKE  # set timeout value
            for i in range(HANDSHAKE_ATTEMPTS):  # handshake attempts
                r = sp.read()  # try reading
                if len(r) == 0:  # if no data
                    self.loading_text.set(
                        self.loading_text_base + f"\nAttempt #{i+1}")  # add attempt counter to label
                    time.sleep(1)  # wait
                else:  # if got data
                    data = r  # store it
                    break  # break from loop

            self.sensorCount = int.from_bytes(
                data, byteorder='big', signed=False)  # convert data to int
            # print(self.sensorCount)
            # set label and title
            self.loading_text.set(
                f"Handshake complete, clearing buffer...\nSensor count: {self.sensorCount}")
            self.title("Connected.")

            # tell board to stop sending data
            sp.write(ConnectionModes.setup.value)

            time.sleep(2)  # awit for board to receive and stop sending

            sp.timeout = TIMEOUT_DEFAULT  # reset timeout to default
            sp.read_until(EOF_BYTES)  # read until EOF
            # print("cleared buffer")
            # tell board to move on to read mode
            sp.write(ConnectionModes.read.value)

        self.window.destroy()  # destroy the loading label

        if self.sensorCount == 0:  # if no sensors detected
            self.quit()  # quit
            # error
            messagebox.showerror(
                "Setup Error", "No sensors are connected to the board! make sure all the connections are secure and in the correct pins. (sensor's OneWire connection pin is Digital 12)")
            raise ConnectionError(
                "No sensors are connected to the board! make sure all the connections are secure and in the correct pins. (sensor's OneWire connection pin is Digital 12)")

        self.lockedSensorData.setValue(
            [0]*self.sensorCount, "handshakeThread")  # init sensor data list

        self.sensorReadThread = threading.Thread(
            target=self.sensorReadThreadFunc, kwargs={"delay": 1})  # get sensorReadThread
        self.sensorReadThread.start()  # start it

        # load sensor setup window
        self.window = SetSensorIDs(self, self.sensorCount)
        self.window.pack(fill='both')
        self.title("Setup Sensors")

    stopReadThread = False

    sensorReadDelay = 1

    def sensorReadThreadFunc(self, delay=-1):

        with self.serPort.getLock("SRTF") as sp:  # access serial port
            sp.timeout = TIMEOUT_THREAD  # set timeout to thread timeout

        if delay != -1:  # if delay is not default
            self.sensorReadDelay = delay  # set delay

        with self.serPort.getLock("SRTF") as sp:  # access serial port
            # read everything until an EOF, meaning clear the buffer entirely
            sp.read_until(EOF_BYTES)

        while not self.stopReadThread:  # as long as no stop was requested
            # print("in sensor thread loop")
            try:
                with self.serPort.getLock("SRTF") as sp:  # access serial port
                    sp: serial.Serial = sp
                    # read sensor count * 2
                    data = sp.read_until(EOF_BYTES, self.sensorCount * 2 + 4)
                    time.sleep(0.1)
                    sp.read_all()
                if self.stopReadThread:
                    break
                if len(data) != self.sensorCount * 2 + 4:  # if any other value of bytes returned
                    # print("No data read |", data)  # error
                    continue  # skip to next loop

                sensorData = data[:self.sensorCount*2]
                timeData = data[self.sensorCount*2:]
                timeData = sum(
                    [timeData[i] << i * 8 for i in range(4)])
                # print(data, sensorData, timeData)
                dt = []
                for i in range(0, len(sensorData), 2):  # iterate over data, in jumps of 2
                    # convert data back to floating point values
                    dt.append(data[i]*1.0 + data[i+1]/10.0)
                if not self.stopReadThread:
                    self.lockedSensorData.setValue(
                        dt, "SRTF")  # set variable to value
                    self.sensorReadDT.setValue(
                        timeData/1000.0, "SRTF")  # set dt
                time.sleep(self.sensorReadDelay)  # wait for sensor delay
            except Exception as e:
                # thread error occurred
                messagebox.showerror(
                    "Sensor Reading Thread Error", f"{str(e)}, {type(e)}")


if __name__ == "__main__":
    app = App()
    app.mainloop()  # start mainloop
